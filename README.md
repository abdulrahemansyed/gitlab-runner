# Gitlab Runner

## Instructions to run Gitlab-Runner

```
# Run the gitlab-runner
docker run -itd --volume /var/run/docker.sock:/var/run/docker.sock --volume /path/to/config/folder:/etc/gitlab-runner --name gitlab-runner jainnish94/gitlab-runner run
```
## To check
```
docker ps | grep gitlab-runner
```

## Register the Runner

```
docker exec -it gitlan-runner /bin/bash
```

1. Run the following command:
```
#gitlab-multi-runner register
```
2. Enter your GitLab instance URL:
```
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
https://gitlab.com
```
3. Enter the token you obtained to register the Runner:
```
Please enter the gitlab-ci token for this runner
xxxxx
```
4. Enter a description for the Runner, you can change this later in GitLab’s UI:
```
Please enter the gitlab-ci description for this runner
[hostname] my-runner
```
5. Enter the tags associated with the Runner, you can change this later in GitLab’s UI:
```
Please enter the gitlab-ci tags for this runner (comma separated):
my-tag,another-tag
```
6. Enter the Runner executor:
```
Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
docker
```
7. If you chose Docker as your executor, you’ll be asked for the default image to be used for projects that do not define one in .gitlab-ci.yml: (e.g. maven:3.6-jdk-8)
```
maven:3.6-jdk-8